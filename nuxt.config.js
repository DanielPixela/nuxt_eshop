export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'impresspic-nuxt',
    titleTemplate: '%s | Impresspic',
    htmlAttrs: {
      lang: 'sk',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.png' },{rel: 'stylesheet', href: 'https://use.typekit.net/dco3pao.css'}],
	  script: [
		  {
			  src: "https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js",
			  crossorigin: 'anonymus',
			  body: true,
			 }
	  ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['@/assets/sass/app.scss'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
	  {src: '@/plugins/vue-agile.js', mode: 'client'},
	  {src: '@/plugins/vuelidate.js'},
	  {src: '@/plugins/sweetalert2.js'},
	  {src: '@/plugins/vue-pagination2.js'},
	  {src: '@/plugins/vue-select.js'},
	  {src: '@/plugins/vue-lightbox.js', mode:'client'},
	  {src: '@/plugins/vue-cropper.js',mode: 'client'},
	  {src: '@/plugins/filters.js'},
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    '@nuxtjs/style-resources',
    '@nuxtjs/fontawesome',
	  '@nuxtjs/moment',
  ],

	moment: {
		defaultLocale: 'sk',
		locales: ['sk']
	},

  // Font awesome 5 icons config
  fontawesome: {
    component: 'fa',
    icons: {
      solid: ['faGlobeEurope','faSearch','faShoppingCart', 'faUpload', 'faCheck', 'faAngleUp','faAngleDown','faAngleRight','faAngleLeft', 'faPhoneAlt', 'faEnvelope', 'faMapMarkerAlt', 'faFileDownload', 'faTimes','faExclamation','faPlus','faMinus','faHands','faBox','faCreditCard','faExchangeAlt', 'faMoneyBill'],
      brands: ['faFacebookF','faInstagram'],
      regular: ['faUser', 'faHeart']
    }
  },

 styleResources: {
   scss: ['@/assets/sass/_functions.scss','@/assets/sass/_mixins.scss','@/assets/sass/_animations.scss','@/assets/sass/_variables.scss']
 },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
	  '@nuxtjs/auth-next',
  ],


  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
	baseURL: process.env.baseUrl,
	  credentials: true
  },

	env: {
		baseUrl: process.env.baseUrl
	},


	auth: {
		redirect: {
			login: '/login',
			logout: '/',
			callback: '/',
			home: '/'
		},

		rewriteRedirects: false,

		strategies: {
			local: {
				token: {
					property: 'data.token'
				},

				user: {
						property: 'data.user',
						autoFetch: false,
				},
				endpoints: {
					login: { url: '/v1/auth/signin', method: 'post' },
					user: { url: '/v1/auth/info', method: 'get' },
					logout: {url: '/v1/auth/invalidate', method: 'post'}
				},

			}
		}
	},


	pageTransition: {
	  name: 'page',
		beforeEnter(el) {
			window.scrollTo({top: 0, behavior: 'instant'});
		},
	},

	loading: {
	  color: '#4F9A95',
		height: '3px'

	},


  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: ['vue-agile'],
	  postcss: false
  },

};

import Vue from 'vue';

export default () => {
	/**
	 * Currency filter
	 */
	Vue.filter('toCurrency', function (value, loc) {
		if (typeof value !== 'number') {
			return value;
		}

		let locale = '';
		let currency = '';

		if (loc === 'SK') {
			locale = 'sk-SK';
			currency = 'EUR';
		} else if (loc === 'CZ') {
			locale = 'cs-CS';
			currency = 'CZK';
		}

		const formatter = new Intl.NumberFormat(locale, {
			style: 'currency',
			currency: currency,
		});
		return formatter.format(value);
	});
};

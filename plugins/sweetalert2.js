// main.js
import Vue from 'vue';
import VueSweetalert2 from 'vue-sweetalert2';

// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css';

// Base styles
const baseStyles = {
	confirmButtonColor: '#41b882',
	showConfirmButton: false,
	timer: 4000,
	timerProgressBar: true,
	toast: true,
	position: 'top-end',
	iconColor: '#ffffff',
};

Vue.use(VueSweetalert2, baseStyles);

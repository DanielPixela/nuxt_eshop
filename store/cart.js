export const state = () => ({
	cart: {
		items: [],
		totalPriceWithTax: null,
		totalPriceWithoutTax: null,
		taxPrice: null,
		totalShippingPrice: 0,
		notification: false,
		shippingMethods: [],
		paymentMethods: [],
		shippingId: null,
		paymentId: null,
		isLoading: false,
	},
});

/**
 * Getters
 * @type {{}}
 */
export const getters = {
	// Get number of items in cart
	cartItems(state) {
		return state.cart.items.length;
	},

	// Loading status
	isLoading(state) {
		return state.cart.isLoading;
	},

	// Notification status
	notification(state) {
		return state.cart.notification;
	},
	// Get shipping methods
	shippingMethods(state) {
		return state.cart.shippingMethods;
	},

	// Get shipping id
	shippingId(state) {
		return state.cart.shippingId;
	},

	// Get payment methods
	paymentMethods(state) {
		return state.cart.paymentMethods;
	},

	// Get payment id
	paymentId(state) {
		return state.cart.paymentId;
	},

	// Get total shipping price
	totalShippingPrice(state) {
		return state.cart.totalShippingPrice;
	},
};

/**
 * Actions
 * @type {{}}
 */
export const actions = {
	// Fetch cart items
	fetchCartItems(context) {
		context.commit('setLoading', true);

		this.$axios
			.get('/v1/cart')
			.then((response) => {
				if (response.data.data.position.length < 1) {
					this.$axios
						.get('/v1/cart/clear')
						.then(() => {})
						.catch((e) => e);
				}
				context.commit('setLoading', false);
				context.commit('setProductsToCart', response.data.data);
				context.commit('setTotalPriceWithTax', response.data.data.total_price.price_with_tax_value);
				context.commit('setTotalPriceWithoutTax', response.data.data.total_price.price_without_tax_value);
				context.commit('setTaxPrice', response.data.data.total_price.tax_price_value);
				context.commit('setShippingId', response.data.data.shipping_type_id);
				context.commit('setPaymentId', response.data.data.payment_method_id);
				context.commit('setTotalShippingPrice', {
					shippingPrice: response.data.data.shipping_price.price_value,
					paymentPrice: response.data.data.payment_price.price_value,
				});
				if (response.data.data.shipping_type_id) {
					context.dispatch('fetchPaymentMethods', response.data.data.shipping_type_id);
				}
			})
			.catch((e) => context.commit('setLoading', false));
	},

	// Add to cart request
	addToCartRequest(context, data) {
		if (data.data.customDimensionId && data.data.flat) {
			this.$axios
				.get(`/v1/cart/add?quantity=${data.quantity}&custom_dimension_id=${data.data.customDimensionId}&flat=${data.data.flat}`)
				.then(() => {
					context.commit('setLoading', false);
					context.commit('showNotification');
					context.dispatch('fetchCartItems');
				})
				.catch((e) => {
					context.commit('setLoading', false);
				});
		} else {
			this.$axios
				.get(`/v1/cart/add?offer_id=${data.data.id}&quantity=${data.quantity}`)
				.then(() => {
					context.commit('setLoading', false);
					context.commit('showNotification');
					context.dispatch('fetchCartItems');
				})
				.catch((e) => {
					context.commit('setLoading', false);
				});
		}
	},

	// Update cart request
	updateCartRequest(context, data) {
		if (data.data.customDimensionId && data.data.flat) {
			this.$axios
				.get(`/v1/cart/update?quantity=${data.quantity}&custom_dimension_id=${data.data.customDimensionId}&flat=${data.data.flat}`)
				.then(() => {
					context.commit('setLoading', false);
					context.commit('showNotification');
					context.dispatch('fetchCartItems');
				})
				.catch((e) => {
					context.commit('setLoading', false);
				});
		} else {
			this.$axios
				.get(`/v1/cart/update?offer_id=${data.data.id}&quantity=${data.quantity}`)
				.then(() => {
					context.commit('setLoading', false);
					context.commit('showNotification');
					context.dispatch('fetchCartItems');
				})
				.catch((e) => {
					context.commit('setLoading', false);
				});
		}
	},

	// Add to cart
	addToCart(context, data) {
		context.commit('setLoading', true);

		let quantity = 1;

		if (context.state.cart.items.find((obj) => obj.item_offer.id === data.id)) {
			const item = context.state.cart.items.find((obj) => obj.item_offer.id === data.id);

			quantity = item.quantity + 1;

			context.dispatch('updateCartRequest', { data: data, quantity: quantity });
		} else {
			context.dispatch('addToCartRequest', { data: data, quantity: quantity });
		}
	},

	// Remove item
	removeItem(context, id) {
		this.$axios
			.get(`/v1/cart/remove?offer_id=${id}&type=offer`)
			.then(() => {
				context.dispatch('fetchCartItems');
			})
			.catch((e) => e);
	},

	// Add quantity
	addQuantity(context, data) {
		context.commit('setLoading', true);

		if (context.state.cart.items.find((obj) => obj.item_offer.id === data.id)) {
			context.dispatch('updateCartRequest', { data: data, quantity: data.quantity });
		} else {
			context.dispatch('addToCartRequest', { data: data, quantity: data.quantity });
		}
	},

	// Increase quantity
	increaseQuantity(context, data) {
		context.commit('setLoading', true);

		let quantity = data.quantity;
		quantity++;

		if (context.state.cart.items.find((obj) => obj.item_offer.id === data.id)) {
			context.dispatch('updateCartRequest', { data: data, quantity: quantity });
		} else {
			context.dispatch('addToCartRequest', { data: data, quantity: quantity });
		}
	},

	// Decrease quantity
	decreaseQuantity(context, data) {
		context.commit('setLoading', true);

		let quantity = data.quantity;
		quantity--;

		if (quantity < 1) {
			context.dispatch('removeItem', data.id);
		}

		if (context.state.cart.items.find((obj) => obj.item_offer.id === data.id)) {
			context.dispatch('updateCartRequest', { data: data, quantity: quantity });
		} else {
			context.dispatch('addToCartRequest', { data: data, quantity: quantity });
		}
	},

	// Fetch shipping methods
	fetchShippingMethods(context) {
		this.$axios
			.$get('/v1/shipping-type')
			.then((response) => {
				context.commit('setShippingMethods', response.data.methods);
				if (context.state.cart.shippingId) {
					context.dispatch('fetchPaymentMethods', context.state.cart.shippingId);
				}
			})
			.catch((e) => e);
	},

	// Add shipping method to cart
	addShippingMethod(context, id) {
		context.commit('setLoading', true);
		this.$axios.$get(`/v1/cart/add-payment-method/null`).catch((e) => e);
		this.$axios
			.$get(`/v1/cart/add-shipping-type/${id}`)
			.then(() => {
				context.dispatch('fetchPaymentMethods', id);
				context.dispatch('fetchCartItems');
			})
			.catch((e) => context.commit('setLoading', false));
	},

	// Add payment method to cart
	addPaymentMethod(context, id) {
		context.commit('setLoading', true);
		this.$axios
			.$get(`/v1/cart/add-payment-method/${id}`)
			.then(() => {
				context.dispatch('fetchCartItems');
				context.commit('setLoading', false);
			})
			.catch((e) => context.commit('setLoading', false));
	},

	// Get payment methods
	fetchPaymentMethods(context, id) {
		const currentShipMethod = context.state.cart.shippingMethods.find((obj) => obj.id === id);

		const paymentMethods = currentShipMethod.shipping_restriction;

		context.commit('setPaymentMethods', paymentMethods);
	},
};

/**
 * Mutations
 * @type {{}}
 */
export const mutations = {
	// Set shipping total price
	setTotalShippingPrice(state, data) {
		state.cart.totalShippingPrice = data.shippingPrice + data.paymentPrice;
	},

	// Set payment methods
	setPaymentMethods(state, data) {
		state.cart.paymentMethods = data;
	},
	// Set shipping methods
	setShippingMethods(state, data) {
		state.cart.shippingMethods = data;
	},

	// Set shipping id
	setShippingId(state, id) {
		state.cart.shippingId = id;
	},

	// Set shipping id
	setPaymentId(state, id) {
		state.cart.paymentId = id;
	},

	// Set products to cart
	setProductsToCart(state, items) {
		state.cart.items = items.position;
	},

	// Set total price
	setTotalPriceWithTax(state, totalPriceWithTax) {
		state.cart.totalPriceWithTax = totalPriceWithTax;
	},

	// Set total price without tax
	setTotalPriceWithoutTax(state, totalPriceWithoutTax) {
		state.cart.totalPriceWithoutTax = totalPriceWithoutTax;
	},

	// Set tax price
	setTaxPrice(state, taxPrice) {
		state.cart.taxPrice = taxPrice;
	},

	// Set loading
	setLoading(state, value) {
		state.cart.isLoading = value;
	},

	// Show notification
	showNotification(state) {
		state.cart.notification = !state.cart.notification;
	},
};
